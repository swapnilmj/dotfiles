set nocompatible              " be iMproved, required

" Switching this off TEMP for bulkcm
" Indentation
"filetype plugin indent on

" " show existing tab with 4 spaces width
" set tabstop=4
" " when indenting with '>', use 4 spaces width
" set shiftwidth=4
" " On pressing tab, insert 4 spaces
" set expandtab

set shortmess+=A

"Plug plugins
call plug#begin('~/.vim/plugged')

Plug 'tpope/vim-sensible'
Plug 'tpope/vim-sleuth'

Plug 'ctrlpvim/ctrlp.vim'
Plug 'easymotion/vim-easymotion'
"Plug 'Shougo/deoplete.nvim'

Plug 'majutsushi/tagbar'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-surround'
"Plug 'zenbro/mirror.vim'
Plug 'chriskempson/base16-vim'
Plug 'preservim/nerdtree', { 'on': 'NERDTreeToggle' }
Plug 'scrooloose/nerdcommenter'
"Plug 'mileszs/ack.vim'
Plug 'rstacruz/sparkup'
Plug 'morhetz/gruvbox'
"Plug 'philips/meink.vim'
"Plug 'fxn/vim-monochrome'
"Plug 'jacoborus/tender.vim'
"Plug 'KKPMW/sacredforest-vim'
Plug 'ajmwagar/vim-deus'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'

"Plug 'udalov/kotlin-vim'
"Plug 'davidoc/todo.txt-vim'
"Plug 'freitass/todo.txt-vim'
"Plug 'ludovicchabant/vim-gutentags'
"Plug 'spolu/dwm.vim'
Plug 'KabbAmine/vCoolor.vim'	
"Plug 'severin-lemaignan/vim-minimap'
"Plug 'wellle/context.vim'
Plug 'godlygeek/tabular'
Plug 'plasticboy/vim-markdown'
"Plug 'chentau/marks.nvim'


" Track the engine.
"Plug 'SirVer/ultisnips'

" Snippets are separated from the engine. Add this if you want them:
Plug 'honza/vim-snippets'

" Trigger configuration. Do not use <tab> if you use https://github.com/Valloric/YouCompleteMe.
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"

Plug 'NLKNguyen/papercolor-theme'

"Plug 'ayu-theme/ayu-vim'
"Plug 'reedes/vim-colors-pencil'
"Plug 'keith/parsec.vim'
"Plug 'sts10/vim-pink-moon'
"Plug 'altercation/vim-colors-solarized'

Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

set termguicolors     " enable true colors support
"let ayucolor="light"  " for light version of theme
"let ayucolor="mirage" " for mirage version of theme
"let ayucolor="dark"   " for dark version of theme
"colorscheme ayu

"Plug 'sindresorhus/focus'
"Plug 'sindresorhus/focus', {'rtp': 'vim'}
"Plug 'w0rp/ale'
"Completion

call plug#end()

"Use ag instead of ack
let g:ackprg = 'rg --vimgrep --smart-case'


"use system clipboard
if has('clipboard')
    if has('unnamedplus') " When possible use + register for copy-paste
        set clipboard=unnamed,unnamedplus
    else " On mac and Windows, use * register for copy-paste
        set clipboard=unnamed
    endif
endif

"" I use this with spf13
"use system clipboard
"set clipboard=unnamedplus "linux

let mapleader = " "

nnoremap d "_d
nnoremap D "_D
vnoremap d "_d

"new tab
nnoremap <leader>t :tabnew<CR>
"delete current buffer
nnoremap <leader>d :bdelete<CR>

" FuGitive
nnoremap <leader>gs :Gstatus<CR>
nnoremap <leader>gd :Gdiff<CR>
nnoremap <leader>gg :Gtabedit : \| set previewwindow<CR>

"Mirror plugin shortcuts
nnoremap <leader>M :MirrorPull<CR>
nnoremap <leader>m :MirrorPush<CR>

"Pane navigation
nnoremap <C-h> <C-w><C-h>
nnoremap <C-l> <C-w><C-l>

nnoremap <leader>l :Limelight!<CR>

"Ctrl+S to save
noremap <silent> <C-S>          :update<CR>
vnoremap <silent> <C-S>         <C-C>:update<CR>
inoremap <silent> <C-S>         <C-O>:update<CR>


noremap <leader>w :update<CR>
noremap <leader>o o<Esc>
noremap <leader>O O<Esc>



"useful(important) maps
nnoremap ; :
nnoremap : :
nnoremap xx Vx
inoremap jj <Esc>

nnoremap <silent><Leader><C-]> <C-w><C-]><C-w>T
nnoremap Y y$

set wrap
"autocmd BufEnter * silent! lcd %:p:h
set autochdir
let NERDTreeChDirMode=2
nnoremap <leader>n :NERDTree .<CR>
nnoremap <C-E> :NERDTreeToggle .<CR>

command!          MenuToggle             if &go=~'m'|set go-=m|else|set go+=m|endif
nnoremap <C-M> :MenuToggle <CR>

nnoremap <leader>b iimport pdb;pdb.set_trace()<CR><Esc>
"let g:pymode_lint = 1

"This unsets the "last search pattern" register by hitting return
"nnoremap <CR> :noh<CR><CR>

set nohlsearch

set nospell

" hide line no.s
set nonu
set linespace=1

"set guifont=DejaVu\ Sans\ Mono\ 9
"set guifont=Inconsolata\ Medium\ 11
"set guifont=Source\ Code\ Pro\ 10
set guifont=Fira\ Code\ Regular\ 9
"set guifont=Liberation\ Mono\ 9

"guifont=Source Code Pro Medium 10

set background=dark
if has('gui_running')
    "colo twilight
    "colo codeschool
    "colo zenburn
    "colo zazen
    "colo manuscript
    "colo slate
    "colo kiwi
    "colo earthsong
    "colo shrek
    "colo slime
    "colo solarized
    "set background=light
    "colo clarity
    "colo desertEx
    "colo colorsbox-material
    "colo colorsbox-stnight
    "colo desert
    "colo mustang
    "colo twilight
    "colo hybrid
    " colo Tomorrow-Night
    "colo base16-tomorrow-night
    "colo base16-grayscale-dark
    "colo base16-gruvbox-dark-medium
    "colo base16-gruvbox-dark-hard

    "colo tender
    
    "let ayucolor="light"  " for light version of theme
    "let ayucolor="mirage" " for mirage version of theme
    "let ayucolor="dark"   " for dark version of theme
    "colorscheme ayu
    
    

    "let g:gruvbox_italic=1 
    "set background=dark
    "let g:gruvbox_contrast_dark='medium'
    "colo gruvbox

    "let g:hybrid_custom_term_colors = 1
    "let g:hybrid_reduced_contrast = 1 " Remove this line if using the default palette.
    "colorscheme hybrid
else
    "colo lucius
    "colo default
    "colo base16-gruvbox-dark-medium
    "colo hybrid-light
endif

"colo base16-solarized-dark
"colo base16-tomorrow-night
colo gruvbox

"let g:airline_solarized_bg='dark'
let g:airline_theme='tomorrow'

    let g:syntastic_mode_map = { 'mode': 'passive',
                               \ 'active_filetypes': [],
                               \ 'passive_filetypes': [] }
"Open CtrlP with MRU mode
nnoremap <F2> :Lines<CR>
nnoremap <F5> :CtrlPMRUFiles<CR>
nnoremap <F6> :CtrlPTag <CR>
nnoremap <F8> :TagbarToggle<CR>
"nnoremap <F6> :colo base16-

"let g:ale_python_auto_pipenv = 1
"nmap <silent> <C-k> <Plug>(ale_previous_wrap)
"nmap <silent> <C-j> <Plug>(ale_next_wrap)


" minimalistic, distraction free coding
set guioptions=

" are these lines really needed ?
set guioptions-=m  "remove menu bar
set guioptions-=T  "remove toolbar
set guioptions-=r  "remove right-hand scroll bar
set guioptions-=L  "remove left-hand scroll bar

"very basic statusline
"set laststatus=0

" In absence of spf13, 
" for tabswitching
nnoremap L gt<CR>
noremap H gT<CR>

nnoremap vir vi(
nnoremap vis vi[
nnoremap vic vi{

"let g:AutoPairs = {}

" Adding this because for just for one py file tagbar isn't working
autocmd BufEnter *.py :setlocal filetype=python
autocmd BufEnter *.jinja :setlocal filetype=htmldjango
set autoread

" Search related
set ignorecase
set smartcase
set incsearch

autocmd VimResized * wincmd =

" For terminal(nvim)
set mouse=a

set lz
set foldmethod=indent   
set foldnestmax=10
set nofoldenable
